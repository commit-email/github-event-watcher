# Copyright (C) 2014-2022  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "cgi/util"
require "net/http"
require "json"

module GitHubEventWatcher
  class WebhookSender
    READ_TIMEOUT_SECONDS = 60 * 10

    def initialize(logger)
      @logger = logger
    end

    def send_event(event, raw_uri)
      case event.type
      when "PushEvent"
        x_github_event = "push"
        tag = "push"
        payload = convert_to_push_event_payload(event)
      when "PullRequestEvent"
        x_github_event = "pull_request"
        tag = "pull-request"
        payload = convert_to_pull_request_event_payload(event)
      else
        @logger.warn("[webhook-sender][send][#{event.type}] " +
                     "<#{event.id}>:<#{event.repository_full_name}> " +
                     "ignore unsupported event type")
        return
      end

      if /%{.+?}/.match?(raw_uri)
        variables = {
          repository_full_name: CGI.escape(event.repository_full_name),
        }
        uri = URI(raw_uri % variables)
      else
        uri = URI(raw_uri)
      end
      @logger.info("[webhook-sender][end-point] <#{uri}>")

      options = {
        :use_ssl => (uri.scheme == "https"),
      }
      begin
        Net::HTTP.start(uri.host, uri.port, options) do |http|
          http.read_timeout = READ_TIMEOUT_SECONDS
          request = Net::HTTP::Post.new(uri.request_uri)
          request["Host"] = uri.hostname
          request["X-GitHub-Event"] = x_github_event
          request["Content-Type"] = "application/json"
          request["User-Agent"] = "GitHub Event Watcher/#{VERSION}"
          request.body = JSON.generate(payload)
          response = http.request(request)
          log_tag = "[webhook-sender][sent][#{tag}]"
          case response
          when Net::HTTPSuccess
            @logger.info("#{log_tag}[success]")
          else
            @logger.error("#{log_tag}[error] <#{response.code}>")
          end
        end
      rescue SystemCallError, Timeout::Error
        tag = "[webhook-sender][send][#{tag}][error]"
        message = "#{tag} Failed to send push event: #{$!.class}: #{$!.message}"
        @logger.error(message)
      end
    end

    private
    def convert_to_push_event_payload(event)
      {
        "ref"    => event.payload["ref"],
        "before" => event.payload["before"],
        "after"  => event.payload["head"],
        "repository" => {
          "url"       => event.repository_url,
          "clone_url" => event.repository_clone_url,
          "html_url"  => event.repository_html_url,
          "name"      => event.repository_name,
          "full_name" => event.repository_full_name,
          "owner" => {
            "name" => event.repository_owner,
          },
        },
      }
    end

    def convert_to_pull_request_event_payload(event)
      overrides = {
        "repository" => {
          "id" => event.repository_id,
          "name" => event.repository_name,
          "full_name" => event.repository_full_name,
          "owner" => {
            "login": event.repository_owner,
          },
        },
        "sender" => event.actor,
      }
      event.payload.merge(overrides)
    end
  end
end
