# Copyright (C) 2014-2023  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module GitHubEventWatcher
  class Event
    def initialize(data)
      @data = data
      @repository = @data["repo"]
    end

    def id
      Integer(@data["id"])
    end

    def type
      @data["type"]
    end

    def actor
      @data["actor"]
    end

    def payload
      @data["payload"]
    end

    def repository_url
      "https://github.com/#{repository_full_name}"
    end

    def repository_clone_url
      "https://github.com/#{repository_full_name}.git"
    end

    def repository_html_url
      "https://github.com/#{repository_full_name}"
    end

    def repository_owner
      owner, _name = repository_full_name.split("/", 2)
      owner
    end

    def repository_name
      _owner, name = repository_full_name.split("/", 2)
      name
    end

    def repository_full_name
      @repository["name"]
    end

    def repository_id
      @repository["id"]
    end
  end
end
